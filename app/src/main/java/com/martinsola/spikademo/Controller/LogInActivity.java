package com.martinsola.spikademo.Controller;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.martinsola.spikademo.Model.ServerResponse;
import com.martinsola.spikademo.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.Object;





import static android.content.ContentValues.TAG;

public class LogInActivity extends MainActivity {

    private EditText userNameEt;
    private EditText passwordEt;
    private String path = "https://spika.chat/api/v3/signin";
    private String apiKey = "GMUwQIHielm7b1ZQNNJYMAfCC508Giof";
    public String accessToken;

    private JSONObject jsonBody = new JSONObject();

    public void sgnIn(View view) throws JSONException {

        String username = userNameEt.getText().toString();
        String password = passwordEt.getText().toString();

        if (!checkInputData(username, password)) {
            Toast.makeText(this, "Please check username or password", Toast.LENGTH_LONG).show();
            return;
        }

        // set Request Json Body
        jsonBody.put("organization","clover");
        jsonBody.put("username",username);
        jsonBody.put("password",password);

        if(isOnline()){

            new MyTask().execute();

        }else {
            Toast.makeText(this,"Network isn't available", Toast.LENGTH_LONG).show();
        }
    }

    protected boolean isOnline(){

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){

            return true;

        }else{
            return false;
        }
    }

    private class MyTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            Log.i(TAG, "do in background");

            try {
                ServerResponse serverData = new ServerResponse(path, apiKey,null,jsonBody);
                accessToken = serverData.accessToken;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return  null;
        }

            @Override
            protected void onPostExecute (Object o){
                super.onPostExecute(o);

                if (accessToken != null) {

                    Intent i = new Intent(getApplicationContext(), SendMessageActivity.class);

                    i.putExtra("acToken", accessToken);
                    startActivity(i);

                    Log.i(TAG, "Gotovi smo sa skidanjem sadrzaja");
                } else {
                    return;
                }
            }

        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        userNameEt = findViewById(R.id.userNameEditText);
        passwordEt = findViewById(R.id.passwordEditText);

    }

    private Boolean checkInputData(String username, String password)
    {
        if (username.length() == 0 || password.length() == 0) {
            return false;
        }

        return true;
    }
}
