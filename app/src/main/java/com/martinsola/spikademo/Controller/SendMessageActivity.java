package com.martinsola.spikademo.Controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.martinsola.spikademo.Model.ServerResponse;
import com.martinsola.spikademo.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static android.content.ContentValues.TAG;

public class SendMessageActivity extends LogInActivity {

    private String path = "https://spika.chat/api/v3/messages";
    private String apiKey = "GMUwQIHielm7b1ZQNNJYMAfCC508Giof";
    private String acToken;
    EditText messageEt;
    String myMessage = "";

    JSONObject jsonBody = new JSONObject();

    public void sendMsgBtn(View view) throws JSONException {

        myMessage= messageEt.getText().toString();

        if (myMessage.length() == 0) {
            Toast.makeText(this, "Please enter message", Toast.LENGTH_LONG).show();
            return;
        }

        jsonBody.put("targetType","3");
        jsonBody.put("messageType","1");
        jsonBody.put("target","5a05ccd4829e64fd1dcd7732");
        jsonBody.put("message",myMessage);



        if(isOnline()){

            new MyTask().execute();

        }else {
            Toast.makeText(this,"Network isn't available", Toast.LENGTH_LONG).show();
        }
    }

    protected boolean isOnline(){

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        }else{
            return false;
        }
    }

    private class MyTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            Log.i(TAG,"do in background");
            try {
                ServerResponse response = new ServerResponse(path,apiKey,acToken,jsonBody);
                Log.i("Server message response", response.responseText);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            Log.i(TAG, "Gotovi smo sa skidanjem sadrzaja");
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        Bundle extras = getIntent().getExtras();
        acToken = extras.getString("acToken");
        messageEt = findViewById(R.id.messageEt);


    }
}
