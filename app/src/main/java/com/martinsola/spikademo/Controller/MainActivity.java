package com.martinsola.spikademo.Controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.martinsola.spikademo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
}
