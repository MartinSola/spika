package com.martinsola.spikademo.Model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static android.content.ContentValues.TAG;

/**
 * Created by Martin on 11/29/2017.
 */

public class ServerResponse {

    private StringBuffer response;
    public String responseText;
    public String accessToken;

    public ServerResponse(String path, String apiKey,String accesToken, JSONObject jsonFormat) throws JSONException {
        try {
            Log.i(TAG,"get web servuces reposne");
            URL url = new URL(path);
            Log.d(TAG, "ServerData: " + path);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestProperty("apikey", apiKey);
            conn.setRequestProperty("access-token", accesToken);

            String json = jsonFormat.toString();

            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Content-Length", Integer.toString(json.length()));
            conn.getOutputStream().write(json.getBytes("UTF8"));

            int responseCode = conn.getResponseCode();

            Log.i(TAG, "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }

                responseText = response.toString();
                Log.i(TAG, "data:" + responseText);

                AccessToken token = new AccessToken(responseText);
                accessToken = token.accessToken;
                Log.i("TOKEN - TOKEN", accessToken);

                in.close();
            }}
        catch(Exception e){
            e.printStackTrace();
        }
}


}
