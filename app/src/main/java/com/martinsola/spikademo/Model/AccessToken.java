package com.martinsola.spikademo.Model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by filipjandrijevic on 23/11/2017.
 */

public class AccessToken {

    public String accessToken = "";

    public AccessToken(String jsonResponse) throws JSONException {

        try {
            JSONObject obj = new JSONObject(jsonResponse);
            String token = obj.getString("access-token");

            if (token.length() == 0) {
                accessToken = "No access token";
            } else {
                accessToken = token;
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}


